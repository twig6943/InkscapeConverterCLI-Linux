# InkscapeConverterCLI-Linux

<img src="https://upload.wikimedia.org/wikipedia/commons/0/0d/Inkscape_Logo.svg" width="200"/>

### A command-line utility to convert files using Inkscape

# Features
• Works with flatpak/appimage/native pkgs

# Usage
```
wget https://gitlab.com/twig6943/InkscapeConverterCLI-Linux/-/raw/main/converter.sh
chmod +x converter.sh
./converter.sh
```

# Disclaimer 
All of this was written by ChatGPT 3.5 

I just wanted to get some files converted.

(Pull Requests are welcome)

## Known issues
For svg to sif conversion see
https://www.youtube.com/watch?v=ytYHqIHow8I
